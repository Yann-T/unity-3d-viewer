﻿using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class AssetReader : MonoBehaviour
{
    [SerializeField]
    private string _name;
    [SerializeField]
    private ModelDisplay _display;

    void Start()
    {
        StartCoroutine(LoadBundle(_name, bundle =>
        {
            if(bundle == null)
            {
                Debug.LogError("Error loading assetBundle");
            } else
            {
                _display.AssetBundle = bundle;
            }
        }));
    }

    /// <summary>
    /// Loads a bundle and pass it to the specified callback function
    /// </summary>
    /// <param name="bundleName">The name of the bundle to request</param>
    /// <param name="callback">The function to which we pass the bundle</param>
    /// <returns></returns>
    IEnumerator LoadBundle(string bundleName, UnityAction<AssetBundle> callback)
    {
        string path = Path.Combine(Application.streamingAssetsPath, "AssetBundles", bundleName.ToLower());
        Debug.Log(path);
#if UNITY_WEBGL && !UNITY_EDITOR
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(path);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.LogError(request.error);
        }
        else
        {
            // Get downloaded asset bundle
            callback(DownloadHandlerAssetBundle.GetContent(request));
        }
#else
        var assetBundleCreateRequest = AssetBundle.LoadFromFileAsync(path);
        yield return assetBundleCreateRequest;
        callback(assetBundleCreateRequest.assetBundle);
#endif
    }
}

﻿using UnityEngine;
using UnityEngine.Events;

public class KeyboardController : MonoBehaviour
{
    public UnityEvent NextEvent { private set; get; } = new UnityEvent();
    public UnityEvent PreviousEvent { private set; get; } = new UnityEvent();

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            PreviousEvent.Invoke();
        }

        if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            NextEvent.Invoke();
        }
    }
}

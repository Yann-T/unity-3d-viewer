﻿using UnityEngine;

public abstract class AssetBundleLoader : ScriptableObject
{
    abstract public AssetBundle LoadAssetBundle();
}

﻿using System.IO;
using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "ScriptableObjects/AssetBundleFromUrlLoader", order = 1)]
public class AssetBundleFromUrlLoader : AssetBundleLoader
{
    [SerializeField]
    private string _bundleName;

    public override AssetBundle LoadAssetBundle()
    {
        // NOTE: This solution does not work when ran from a browser in WebGL
        var request = UnityWebRequestAssetBundle.GetAssetBundle(Path.Combine(Application.streamingAssetsPath, "AssetBundles", _bundleName));
        request.SendWebRequest();
        while (!request.isDone) ;    // Obviously this should be handled asynchronously and not like this (but it's an example)
        return DownloadHandlerAssetBundle.GetContent(request);
    }

}


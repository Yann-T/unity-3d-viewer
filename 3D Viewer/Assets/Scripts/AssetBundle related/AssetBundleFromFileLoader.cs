﻿using System.IO;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObjects", menuName = "ScriptableObjects/AssetBundleFromFileLoader", order = 1)]
public class AssetBundleFromFileLoader : AssetBundleLoader
{
    [SerializeField]
    private string _bundleName;

    public override AssetBundle LoadAssetBundle()
    {
        return AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "AssetBundles", _bundleName));
    }
}

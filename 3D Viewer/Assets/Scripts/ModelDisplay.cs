﻿using System;
using UnityEngine;

public class ModelDisplay : MonoBehaviour
{
    [SerializeField]
    private AssetBundleLoader _loader;      // The object used to load the AssetBundle
    [SerializeField]
    private KeyboardController _controller; // The controller who raises events when we need to change model

    private AssetBundle _assetBundle;       // The assetBundle containing the objects to display
    private int _currentModelIndex = 0;     // The index of the currently displayed model in the AssetBundle
    private int _totalModels;               // The total number of models in the AssetBundle

    public AssetBundle AssetBundle { 
        get { 
            return _assetBundle; 
        }
        set {
            _assetBundle = value;
            _totalModels = _assetBundle.GetAllAssetNames().Length;

            DisplayModelByIndex(_currentModelIndex);
        } 
    }

    private void Start()
    {
        InitEvents();
    }

    /// <summary>
    /// Displays the next model
    /// </summary>
    private void DisplayNextModel()
    {
        if(_assetBundle == null)
        {
            return;
        }

        _currentModelIndex = (_currentModelIndex + 1) % _totalModels;
        DisplayModelByIndex(_currentModelIndex);
    }

    /// <summary>
    /// Displays the previous model
    /// </summary>
    private void DisplayPreviousModel()
    {
        if (_assetBundle == null)
        {
            return;
        }

        _currentModelIndex = _currentModelIndex - 1;
        _currentModelIndex = (_currentModelIndex < 0) ? _currentModelIndex = _totalModels - 1 : _currentModelIndex;
        DisplayModelByIndex(_currentModelIndex);
    }

    /// <summary>
    /// Displays the specified model
    /// </summary>
    /// <param name="index"> The index of the model to display</param>
    private void DisplayModelByIndex(int index)
    {
        if(index < 0 || index > _totalModels)
        {
            throw new IndexOutOfRangeException();
        }

        RemovePreviousModel();
        InstantiateModel(_assetBundle.GetAllAssetNames()[index]);
    }

    /// <summary>
    /// Instanciates the model by name
    /// </summary>
    /// <param name="name">The name of the model to instantiate</param>
    private void InstantiateModel(string name)
    {
        var prefab = _assetBundle.LoadAsset<GameObject>(name);
        var instance = Instantiate(prefab, transform);

        Renderer renderer = instance.GetComponentInChildren<Renderer>();
        var offset = renderer.bounds.center - transform.position;
        instance.transform.localPosition = -offset;
    }

    /// <summary>
    /// Removes the children of the gameobject
    /// </summary>
    private void RemovePreviousModel()
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    /// <summary>
    /// Initialize the event we need to listen to
    /// </summary>
    private void InitEvents()
    {
        _controller.NextEvent.RemoveListener(DisplayNextModel);
        _controller.PreviousEvent.RemoveListener(DisplayPreviousModel);

        _controller.NextEvent.AddListener(DisplayNextModel);
        _controller.PreviousEvent.AddListener(DisplayPreviousModel);
    }

}
